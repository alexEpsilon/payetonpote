<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Participant;
use App\Entity\Payment;
use App\Entity\Campaign;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;



class PaymentController extends AbstractController
{
    /**
     * @Route("/charge", name="payment")
     */
    public function charge(Request $request) : Response
    {
        $campaign_id = $request->request->get('campaign_id');
        $amount = (int)$request->request->get('amount')*100;
        

        try{
            \Stripe\Stripe::setApiKey('sk_test_nOXZodLZ9KOfQyFBL8sU5zou');
        $charge = \Stripe\Charge::create(['amount' => $amount, 'currency' => 'eur', 'source' => $request->request->get("stripeToken")]);
        
        }
        catch(\Exception $e){
            dd('erreur payment');
        }
        
        $participant = new Participant();

        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));

        $campaign = $this->getDoctrine()->getRepository(Campaign::class)->find($campaign_id);
        $participant->setCampaign($campaign);
        

        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        
        $em->flush();

        $payment = new Payment();
        $payment->setParticipant($participant);
        $payment->setAmount($amount);
        $em = $this->getDoctrine()->getManager();
        $em->persist($payment);
        $em->flush();

        return $this->redirectToRoute('campaign_show', [ 'id' => $campaign_id]);

    }
}
