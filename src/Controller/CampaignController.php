<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Participant;
use App\Entity\Payment;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{

    /**
     * @Route("/new", name="campaign_new", methods="GET|POST")
     */
    public function new(Request $request) : Response
    {
        $campaign = new Campaign();
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $id = md5(random_bytes(50));
            $campaign->setId($id);
            $campaign->setName($request->request->get('name'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($campaign);
            $em->flush();

            return $this->redirectToRoute('campaign_show', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/new.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET")
     */
    public function show(Campaign $campaign) : Response
    {
        $query = 'SELECT participant.*, payment.amount as payment_amount, spending.amount as spending_amount, spending.label as spending_label 
        FROM participant 
        LEFT JOIN payment ON payment.participant_id = participant.id
        LEFT JOIN spending ON spending.participant_id = participant.id
        WHERE campaign_id = "' . $campaign->getId() . '"';
        
        $statement = $this->getDoctrine()
            ->getManager()
            ->getConnection()
            ->prepare($query);
        $statement->execute();
        
        $participantWithParticipations = $statement->fetchAll();
        $participants = count($participantWithParticipations);
        $totalAmount = 0;
        for ($i = 0; $i <= ($participants - 1); $i++) {
            $totalAmount += $participantWithParticipations[$i]['payment_amount'];
            $totalAmount += $participantWithParticipations[$i]['spending_amount'];
        }
        $totalAmount2 = $totalAmount / 100;
        $goal = $totalAmount2 / $campaign->getGoal() * 100;

        return $this->render('campaign/show.html.twig', compact('campaign', 'participantWithParticipations', 'participants', 'totalAmount2', 'goal'));
    }

    /**
     * @Route("/{id}/pay", name="campaign_pay", methods="GET|POST")
     */
    public function pay(Campaign $campaign) : Response
    {
        $amountParticipant = $_GET['amountParticipant'] ?? "";

        return $this->render('campaign/pay.html.twig', compact('campaign', 'amountParticipant'));
    }

    /**
     * @Route("/{id}/spend", name="campaign_spend", methods="GET|POST")
     */
    public function spend(Campaign $campaign) : Response
    {
        $amountParticipant = $_GET['amountParticipant'];
        return $this->render('campaign/spend.html.twig', compact('campaign', 'amountParticipant'));
    }

    /**
     * @Route("/{id}/edit", name="campaign_edit", methods="GET|POST")
     */
    public function edit(Request $request, Campaign $campaign) : Response
    {
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('campaign_edit', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/edit.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign) : Response
    {
        if ($this->isCsrfTokenValid('delete' . $campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }
}
